let baseValue = 0;
const baseValueObject = document.getElementById('base-value');
const result = document.getElementById('result');
result.value = '0';

function changeCurrentValue(value) {
    if (result.value[0] == '0') {
        result.value = '';
    }
    result.value += value;
}

function plus() {
    baseValue += parseInt(result.value);
    baseValueObject.innerText = baseValue;
    result.value = '0';
}

function checkout() {
    plus();
    result.value = baseValue;
    baseValue = 0;
}